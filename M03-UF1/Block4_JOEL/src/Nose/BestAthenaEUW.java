package Nose;

import java.util.Scanner;

public class BestAthenaEUW {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			
			int files = src.nextInt();
			int cols = src.nextInt();
			
			//omplim la matriu
			int [][] mat = new int[files][cols];
			
			for (int i = 0; i < files; i++)
				for (int j = 0; j < cols; j++)
					mat[i][j] = src.nextInt();
			
			int x = src.nextInt();
			int y = src.nextInt();
			
			boolean correcte = (mat[x][y] == 1);
			if (correcte) {
				int maxf = x + 1;
				int maxc = y + 1;
				int minf = x - 1;
				int minc = y - 1;
				if (maxf == files) maxf--;
				if (maxc == cols) maxc--;
				if (minf < 0) minf ++;
				if (minc < 0) minc ++;
				int cont = 0;
				
				for (int i = minf; i <= maxf; i++)
					for (int j = minc; j <= maxc; j++) 
						if (!(i == x && j == y) && mat[i][j] == 2) cont++;
				System.out.println(cont);
			}
			else System.out.println("-1");
		}
	}

}
